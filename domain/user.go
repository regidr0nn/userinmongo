package domain

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	_id primitive.ObjectID `json:"id,omitempty" bson:"_id"`
	ID uint64 `json:"id" bson:"id"`
	FirstName string `json:"first_name" bson:"first_name"`
	LastName string `json:"last_name" bson:"last_name"`
	Email string `json:"email" bson:"email"`
}

